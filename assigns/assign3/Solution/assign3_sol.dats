(*
** Assignment 3
** It is due Wednesday,
** the 28th of September, 2016
*)

(* ****** ****** *)
//
// HX: Try typechecking:
//
// patscc -tcats assign3_sol.dats
//
(* ****** ****** *)

#include "./../assign3.dats"

(* ****** ****** *)
//
// conj_associate2
//
primplement
conj_associate2
  (pf0) = let
  prval pfA = conj_elim_l(pf0)
  prval pfB = conj_elim_l(conj_elim_r(pf0))
  prval pfC = conj_elim_r(conj_elim_r(pf0))
in
  conj_intr(conj_intr(pfA, pfB), pfC)
end // end of [conj_associate2]
//
(* ****** ****** *)
//
// disj_associate2
//
primplement
disj_associate2
  (pf0) =
(
case+ pf0 of
| disj_intr_l(pfA) =>
    disj_intr_l(disj_intr_l(pfA))
  // end of [disj_intr_l]
| disj_intr_r(pfBC) =>
  (
    case+ pfBC of
    | disj_intr_l(pfB) =>
        disj_intr_l(disj_intr_r(pfB))
      // disj_intr_l
    | disj_intr_r(pfC) => disj_intr_r(pfC)
  ) (* end of [disj_intr_r] *)
) (* end of [conj_associate2] *)
//
(* ****** ****** *)
//
// DistLaw_conjdisj
//
primplement
DistLaw_conjdisj
  (pf0) = let
//
prval pfA = conj_elim_l(pf0)
prval pfBC = conj_elim_r(pf0)
//
in
//
case+ pfBC of
| disj_intr_l(pfB) => disj_intr_l(conj_intr(pfA, pfB))
| disj_intr_r(pfC) => disj_intr_r(conj_intr(pfA, pfC))
//
end // end of [DistLaw_conjdisj]
//
(* ****** ****** *)
//
// deMorgan_disj
//
primplement
deMorgan_disj
  {A,B}(pf0) =
  conj_intr
  (
    neg_intr(lam(pfA: A) => neg_elim(pf0, disj_intr_l(pfA)))
  , neg_intr(lam(pfB: B) => neg_elim(pf0, disj_intr_r(pfB)))
  ) (* end of [conj_intr] *)
//
(* ****** ****** *)
//
// deMorgan_conj
//
primplement
deMorgan_conj
  {A,B}(pf0) = let
//
prval lem = LEM{A}()
//
in
//
case+ lem of
| disj_intr_l(pfA) =>
  disj_intr_r(neg_intr(lam(pfB: B) => neg_elim(pf0, conj_intr(pfA, pfB))))
| disj_intr_r(pfA_neg) => disj_intr_l(pfA_neg)
//
end // end of [deMorgan_conj]
//
(* ****** ****** *)

(* end of [assign3_sol.dats] *)
