(*
** Solution to assign2.dats
*)

(* ****** ****** *)
//
#include
"share/atspre_define.hats" // defines some names
#include
"share/atspre_staload.hats" // for targeting C
#include
"share/HATS/atspre_staload_libats_ML.hats" // for ...
//
(* ****** ****** *)

#include "./../assign4.dats"

(* ****** ****** *)
//
extern
fun // for skipping non-letters
skip : stream(char) -> stream(char)
extern
fun // for reading a word from the given stream
read : stream(char) -> (list0(char), stream(char))
//
(* ****** ****** *)

implement
stream_wordizing
  (cs) = $delay
(
let
//
val cs = skip(cs)
val (cs1, cs2) = read(cs)
//
in
//
case+ cs1 of
| list0_nil() =>
  stream_nil(*void*)
| list0_cons _ =>
  stream_cons(string_make_rlist(cs1), stream_wordizing(cs2))
//
end
) (* end of [stream_wordizing] *)

(* ****** ****** *)

(* end of [assign4_hint.dats] *)
