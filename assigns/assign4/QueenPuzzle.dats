(* ****** ****** *)

(*
** Author: Hongwei Xi
** Authoremail: gmhwxiATgmailDOTcom
** Time: the 26th of July, 2016
*)

(* ****** ****** *)
//
#include
"share/atspre_define.hats"
#include
"{$LIBATSCC2JS}/staloadall.hats"
//
(* ****** ****** *)

#define N 8 // HX: it can be changed

(* ****** ****** *)
//
// A chess board configuration for the
// 8-queen puzzle can be represented as
// a list of integers. For instance, the
// list (0, 3, 5, 7, 2) represents a board
// containing 5 queen pieces that are located
// at positions (0, 0), (1, 3), (2, 5), (3, 7)
// and (4, 2):
//
(*
   Q . . . . . . .
   . . . Q . . . .
   . . . . . Q . .
   . . . . . . . Q
   . . Q . . . . .
   . . . . . . . .
   . . . . . . . .
   . . . . . . . .
*)
//
// Note that both the row number and column
// number start from 0 and end at 7.
//
// A solution to the 8-queen puzzle is a board
// configuration that contains 8 queen pieces such
// that no piece can attach any other pieces
//
(* ****** ****** *)
//
// HX: 30 points
//
// The following function QueenPuzzle_solve returns
// a stream of all the solutions to the 8-queen puzzle
//
extern
fun
QueenPuzzle_solve(): stream(list0(int)) = "mac#"
//
(* ****** ****** *)
//
// HX: 10 bonus points
//
// The following function QueenPuzzle_solve2 returns
// a stream of all the partial solutions to the 8-queen
// puzzle that are lexicographically ordered. Note that
// a partial solution may contain less than 8-queen pieces
// (occupying the first few rows)
//
extern
fun
QueenPuzzle_solve2(): stream(list0(int)) = "mac#"
//
(* ****** ****** *)

(* end of [QueenPuzzle.dats] *)
