(*
** Game-of-24
*)

(* ****** ****** *)
//
#include
"share/atspre_define.hats"
#include
"{$LIBATSCC2JS}/staloadall.hats"
//
(* ****** ****** *)
//
#define
ATS_MAINATSFLAG 1
#define
ATS_DYNLOADNAME "Game24_dynload"
//
#define
ATS_STATIC_PREFIX "_Game_of_24_"
//
(* ****** ****** *)
//
stadef
tup(a:t@ype, b:t@ype) = $tup(a, b)
stadef
tup(a:t@ype, b:t@ype, c:t@ype) = $tup(a, b, c)
//
(* ****** ****** *)
//
// HX: 10 points for both
//
extern
fun
{a:t0p}
choose1{n:int | n >= 1}
(
  xs: list(a, n)
) : stream(tup(a, list(a, n-1)))
extern
fun
{a:t0p}
choose2{n:int | n >= 2}
(
  xs: list(a, n)
) : stream(tup(a, a, list(a, n-2)))
//
(* ****** ****** *)

typedef dbl = double

(* ****** ****** *)

#define EPSILON 0.000001

(* ****** ****** *)

fun is_0(x: dbl) = abs(x) < EPSILON
fun is_24(x: dbl) = abs(x-24.0) < EPSILON

(* ****** ****** *)
//
extern
fun
arithops(x: dbl, y: dbl): list0(dbl)
//
(* ****** ****** *)
//
implement
arithops(x, y) =
  reverse(res) where
{
  val res = nil0()
  val res = cons0(x + y, res)
  val res = cons0(x - y, res)
  val res = cons0(y - x, res)
  val res = cons0(x * y, res)
  val res = (if is_0(y) then res else cons0(x / y, res)): list0(dbl)
  val res = (if is_0(x) then res else cons0(y / x, res)): list0(dbl)
}
//
(* ****** ****** *)
//
// HX: 10 points for both
//
extern
fun
solve_one{n:int|n >= 2}(list(dbl, n)): stream(list(dbl, n-1))
extern
fun
solve_ones{n:int|n >= 2}(stream(list(dbl, n))): stream(list(dbl, n-1))
//
(* ****** ****** *)
//
extern
fun
Game24_play
(
  n1: int, n2: int, n3: int, n4: int
) : bool = "mac#"
//
(* ****** ****** *)

macdef i2d = int2double

(* ****** ****** *)
//
#define
sing(x)
list_cons(x, list_nil())
//
(* ****** ****** *)

implement
Game24_play
(
  n1, n2, n3, n4
) = let
//
  val xs = nil()
  val xs = cons(i2d(n1), xs)
  val xs = cons(i2d(n2), xs)
  val xs = cons(i2d(n3), xs)
  val xs = cons(i2d(n4), xs)
//
  val xss = solve_one(xs)
  val xss = solve_ones(xss)
  val xss = solve_ones(xss)
in
  xss.exists()(lam(xs) => let val+sing(x) = xs in is_24(x) end)
end // end of [Game24_play]

(* ****** ****** *)

%{$
//
Game24_dynload();
//
alert("Game24(3, 3, 8, 8) = " + String(Game24_play(3, 3, 8, 8)))
alert("Game24(1, 3, 7, 11) = " + String(Game24_play(1, 3, 7, 11)))
alert("Game24(3, 5, 7, 13) = " + String(Game24_play(3, 5, 7, 13)))
alert("Game24(4, 4, 10, 10) = " + String(Game24_play(4, 4, 10, 10)))
alert("Game24(5, 5, 7, 11) = " + String(Game24_play(5, 5, 7, 11)))
alert("Game24(5, 7, 7, 11) = " + String(Game24_play(5, 7, 7, 11)))
//
%} // end of [%{$]

(* ****** ****** *)


(* end of [Game-of-24.dats] *)
