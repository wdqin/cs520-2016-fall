(*
** Game-of-24
*)

(* ****** ****** *)
//
#include
"./../Game-of-24.dats"
//
(* ****** ****** *)
//
fun{}
stream_sing
  {a:t0p}
(
  x0: a
) : stream_con(a)
  = stream_cons(x0, stream_make_nil())
//
(* ****** ****** *)
//
implement
{a}(*tmp*)
choose1{n}(xs) = $delay
(
case+ xs of
| cons(x0, xs) =>
  (
  case+ xs of
  | nil() => stream_sing($tup(x0, xs))
  | cons _ =>
    stream_cons(
      $tup(x0, xs)
    , (choose1(xs)).map(TYPE{$tup(a,list(a,n-1))})(lam($tup(x1, xs)) => $tup(x1, cons(x0, xs)))
    )(* stream_cons *)
  )
)
//
implement
{a}(*tmp*)
choose2(xs) = $delay
(
//
case+ xs of
| cons(x0, xs) =>
  (
    case+ xs of
    | cons(x1, nil()) => stream_sing($tup(x0, x1, nil()))
    | _ (* |xs| >= 2 *) =>>
      !(stream_append
        (
          stream_map_cloref(choose1(xs), lam($tup(x1, xs)) => $tup(x1, x0, xs))
        ,
          stream_map_cloref(choose2(xs), lam($tup(x1, x2, xs)) => $tup(x1, x2, cons(x0, xs)))
        )
       )
  )
//
) (* end of [choose2] *)
//
(* ****** ****** *)
//
implement
solve_one
  {n}(xs) = let
//
val
x_y_zs_list =
  choose2(xs)
//
in
//
stream_concat
(
x_y_zs_list.map
(
TYPE
{
stream(list(dbl,n-1))
}(*TYPE*)
)
(
lam
(
  $tup(x, y, zs)
) =>
stream_make_list0
(
  (arithops(x, y)).map(TYPE{list(dbl,n-1)})(lam z => cons(z, zs))
)
)(* x_y_zs_list.map *)
)
//
end // end of [solve_one]
//
(* ****** ****** *)
//
implement
solve_ones
  {n}(xss) =
(
  stream_concat
  (
    (xss).map(TYPE{stream(list(dbl,n-1))})(lam xs => solve_one(xs))
  )
) (* end of [solve_ones] *)
//
(* ****** ****** *)

(* end of [Game-of-24_sol.dats] *)

