(*
//
** Midterm
//
** Course: BU CAS CS520
//
** Out: noon on the 26th of October
** Due: 11:59pm on the 26th of October
//
*)

(* ****** ****** *)
//
// HX: 20 points
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
// HX: 10 points
// Each integer can be represented as a stream of digits.
// For instance, the integer
// 123456789 can be represented as 9, 8, 7, 6, 5, 4, 3, 2, 1
//
// Given a stream of digits ds, let rep(ds) be the integer
// value represented by ds.
//
(* ****** ****** *)
//
// HX: 5 points
//
// Please implement stream_int_add such that
// rep(x) + rep(y) = rep(stream_int_add(x, y))
//
extern
fun
stream_int_add(x: stream(int), y: stream(int)): stream(int)
//
(* ****** ****** *)
//
// HX: 10 points
//
// Please implement stream_int_add such that
// rep(x) * rep(y) = rep(stream_int_mul(x, y))
//
extern
fun
stream_int_mul(x: stream(int), y: stream(int)): stream(int)
//
(* ****** ****** *)

(* end of [Q1-2.dats] *)
