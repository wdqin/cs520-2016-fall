//
#include "./../Q1-2.dats"
//
(* ****** ****** *)

fun
cadd
(
c: int, xs: stream(int)
): stream(int) = $delay
(
case+ !xs of
| stream_nil() =>
  stream_sing<int>(c)
| stream_cons(x, xs) => let
    val xc = x + c
    val c0 = xc / 10
    val d0 = xc % 10
  in
    stream_cons(d0, cadd(c0, xs))
  end
)

fun
cadd_if
(
c: int, xs: stream(int)
): stream(int) =
  if c = 0 then xs else cadd(c, xs)


fun
kmul
(
k: int, xs: stream(int)
): stream(int) = $delay
(
case+ !xs of
| stream_nil
    () =>
    stream_nil(*void*)
| stream_cons
    (x, xs) => let
    val kx = k * x
    val c0 = kx / 10
    val d0 = kx % 10
  in
    stream_cons(d0, cadd_if(c0, kmul(k, xs)))
  end
)
  
fun
kmul_if
(
k: int, xs: stream(int)
): stream(int) =
(
  if k = 0
    then stream_make_nil()
    else if k = 1 then xs else kmul(k, xs)
  // end of [if]
)

(* ****** ****** *)

implement
stream_int_add
  (xs, ys) = $delay
(
case+ !xs of
| stream_nil() => !ys
| xs_con as
  stream_cons(x, xs2) =>
  (
    case+ !ys of
    | stream_nil
        () => xs_con
    | stream_cons
        (y, ys2) => let
        val xy = x + y
        val c0 = xy / 10
        val d0 = xy % 10
      in
        stream_cons(d0, cadd_if(c0, stream_int_add(xs2, ys2)))
      end // end of [stream_cons]
  )
)

(* ****** ****** *)

implement
stream_int_mul
  (xs, ys) = $delay
(
case+ !xs of
| stream_nil() => stream_nil()
| stream_cons(x, xs) =>
  !(stream_int_add(kmul_if(x, ys), stream_make_cons(0, stream_int_mul(xs, ys))))
)

(* ****** ****** *)

implement main0 () = ()
  
(* ****** ****** *)

(* end of [Q1-2_sol.dats] *)
