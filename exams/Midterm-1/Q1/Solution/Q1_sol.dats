//
#include "./../Q1.dats"
//
(* ****** ****** *)

implement main0 () = ()
  
(* ****** ****** *)

implement
{a}(*tmp*)
stream_enumerate
  (xss) = let
//
fun
merge
  (xs: stream(a), ys: stream(a)) : stream(a) =
$delay
(
  let val-stream_cons(x, xs) = !xs in stream_cons(x, merge(ys, xs)) end
)
//
in
//
$delay
(
let val-stream_cons(xs, xss) = !xss in !(merge(xs, stream_enumerate<a>(xss))) end
) (* end of [delay] *)
//
end (* end of [stream_enumerate] *)

(* ****** ****** *)

(* end of [Q1_sol.dats] *)
