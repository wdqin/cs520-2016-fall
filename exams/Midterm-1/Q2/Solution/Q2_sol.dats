(* ****** ****** *)
//
#include "./../Q2.dats"
//
(* ****** ****** *)

primplmnt
Konstant(pfA) = impl_intr(lam pfB => pfA)

(* ****** ****** *)

primplmnt
Substitute
  (pfABC) =
impl_intr
(
  lam pfAB =>
  impl_intr(lam(pfA) => impl_elim(impl_elim(pfABC, pfA), impl_elim(pfAB, pfA)))
  
) (* impl_intr *)

(* ****** ****** *)
//
primplmnt
Contrapos(pfAB) =
  impl_intr(lam pfBC => impl_intr(lam pfA => impl_elim(pfBC, impl_elim(pfAB, pfA))))
//
(* ****** ****** *)

implement main0 () = ()
  
(* ****** ****** *)

(* end of [Q2_sol.dats] *)
