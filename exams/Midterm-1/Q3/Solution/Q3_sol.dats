//
#include "./../Q3.dats"
//
(* ****** ****** *)

stacst sgn : int -> int
stacst fib : int -> int
stacst luc : int -> int

(* ****** ****** *)

extern
prfun
lemma_sgn_0 : () -<prf> [sgn(0)==1] void
extern
prfun
lemma_sgn_1 : {n:int | n >= 1}() -<prf> [sgn(n)+sgn(n-1)==0] void

(* ****** ****** *)

extern
prfun
lemma_fib_0 : () -<prf> [fib(0)==0] void
extern
prfun
lemma_fib_1 : () -<prf> [fib(1)==1] void
extern
prfun
lemma_fib_2 : {n:int | n >= 2} () -<prf> [fib(n)==fib(n-2)+fib(n-1)] void

(* ****** ****** *)

extern
prfun
lemma_luc_0 : () -<prf> [luc(0)==2] void
extern
prfun
lemma_luc_1 : () -<prf> [luc(1)==1] void
extern
prfun
lemma_luc_2 : {n:int | n >= 2} () -<prf> [luc(n)==luc(n-2)+luc(n-1)] void

(* ****** ****** *)
//
prfun
lemma_FibLucas
  {n:nat} .<n, 0>.
(
  // argumentless
) : [luc(n)*luc(n)==5*fib(n)*fib(n)+4*sgn(n)] void = let
//
prval () = lemma_sgn_0()
//
prval () = lemma_fib_0()
prval () = lemma_luc_0()
//
prval () = lemma_fib_1()
prval () = lemma_luc_1()
//
in
//
sif
(n==0)
then ()
else (
//
sif
(n==1)
then
{
  prval () = lemma_sgn_1{n}()
} (* end of [then] *)
else let
  prval () = lemma_sgn_1{n}()
  prval () = lemma_fib_2{n}()
  prval () = lemma_luc_2{n}()
  prval () = lemma_FibLucas{n-1}()
  prval () = lemma_sgn_1{n-1}()
  prval () = lemma_FibLucas{n-2}()
  prval () = lemma_FibLucas_2{n-2}()
in
  // nothing  
end // end of [let] // end-of-else
//
) (* else *)
//
end // end of [lemma_FibLucas]

and
lemma_FibLucas_2
  {n:nat} .<n, 1>.
(
  // argumentless
) : [luc(n)*luc(n+1)==5*fib(n)*fib(n+1)+2*sgn(n)] void = let
//
prval () = lemma_sgn_0()
//
prval () = lemma_fib_0()
prval () = lemma_luc_0()
//
prval () = lemma_fib_1()
prval () = lemma_luc_1()
//
in
//
sif
(n==0)
then
{
  prval () = lemma_sgn_1{n+1}()
} (* end of [then] *)
else
{
  prval () = lemma_sgn_1{n}()
  prval () = lemma_sgn_1{n+1}()
  prval () = lemma_fib_2{n+1}()
  prval () = lemma_luc_2{n+1}()
  prval () = lemma_FibLucas{n}()
  prval () = lemma_FibLucas_2{n-1}()
}
//
end // end of [lemma_FibLucas_2]

(* ****** ****** *)

implement main0 () = ()
  
(* ****** ****** *)

(* end of [Q3_sol.dats] *)
