//
#include "./../Q4-2.dats"
//
(* ****** ****** *)
//
extern
fun
{a:t@ype}
{b:t@ype}
mylist_map_cloref
  {n:int}(list(a, n), a -<cloref1> b): list(b, n)
//
implement
{a}{b}
mylist_map_cloref
  (xs, fopr) = list_vt2t(list_map_cloref<a><b>(xs, fopr))
//
(* ****** ****** *)
//
(*
extern
fun
mymat_transpose
  {m,n:nat}(mymat(T, m, n)): mymat(T, n, m)
*)
//
(*
extern
fun
mul_mymat_mymat{p,q,r:nat}
  (M1: mymat(T, p, q), M2: mymat(T, q, r)): mymat(T, p, r)
*)
//
(* ****** ****** *)

implement
mymat_transpose
  {m,n}(M) = let
//
val-list_cons(xs, _) = M
//
in
//
case+ xs of
| list_nil _ =>
    list_nil()
| list_cons _ =>
    list_cons
    (
      mylist_map_cloref<list(T,n)><T>(M, lam(xs) => list_head(xs))
    ,
      mymat_transpose{m,n-1}
      (
        mylist_map_cloref<list(T,n)><list(T,n-1)>(M, lam(xs) => list_tail(xs))
      )
    ) (* list_cons *)
//
end // end of [mymat_transpose]

(* ****** ****** *)

implement
mul_mymat_mymat
  {p,q,r}(M1, M2) = let
//
fun
dotprod{n:int}
(
  xs: list(T, n), ys: list(T, n), res: T
) : T =
(
case+ (xs, ys) of
| (list_nil(), list_nil()) => res
| (list_cons(x, xs), list_cons(y, ys)) => dotprod(xs, ys, res+x*y)
)
//
val N2 = mymat_transpose(M2)
//
in
//
mylist_map_cloref<list(T, q)><list(T,r)>
  (M1, lam(xs) => mylist_map_cloref<list(T,q)><T>(N2, lam(ys) => dotprod(xs, ys, 0.0)))
//
end // end of [mul_mymat_mymat]

(* ****** ****** *)

implement main0 () = ()
  
(* ****** ****** *)

(* end of [Q4-2_sol.dats] *)

