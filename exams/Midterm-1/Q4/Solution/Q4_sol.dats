//
#include "./../Q4.dats"
//
(* ****** ****** *)
//
// [Q4_sol] is yet to be implemented
//
(* ****** ****** *)

implement
{a}(*tmp*)
mylist_cons
(x, xs) =
aux(x, xs) where
{
//
#define nil mylist_nil
//
fun
aux:
$d2ctype
(
mylist_cons<a>
) = lam(x, xs) =>
(
case+ xs of
| mylist_nil() => mylist_cons1(x, nil(), nil())
| mylist_cons0(xs1, xs2) => mylist_cons1(x, xs1, xs2)
| mylist_cons1(y, xs1, xs2) => mylist_cons0(aux(x, xs1), aux(y, xs2))
)
} (* end of [mylist_cons] *)

(* ****** ****** *)

implement
{a}(*tmp*)
mylist_uncons
(xs) =
aux(xs) where
{
//
#define nil mylist_nil
//
fun
aux:
$d2ctype
(
mylist_uncons<a>
) = lam(xs) =>
(
case+ xs of
| mylist_cons0
    (xs1, xs2) => let
    val (x, xs1) = aux(xs1)
    val (y, xs2) = aux(xs2)
  in
    (x, mylist_cons1(y, xs1, xs2))
  end // end of [mylist_cons0]
| mylist_cons1(x, xs1, xs2) =>
  (
    case+ xs1 of
    | nil() => (x, mylist_nil)
    | _(*non-nil*) =>> (x, mylist_cons0(xs1, xs2))
  )
)
} (* end of [mylist_uncons] *)

(* ****** ****** *)

implement main0 () = ()
  
(* ****** ****** *)

(* end of [Q4_sol.dats] *)
