//
#include "./../Q5.dats"
//
(* ****** ****** *)

implement
sublist_sum
  {m}(xs, m) = $delay
(
//
if
m = 0
then
stream_sing(intlist_nil())
else
(
case+ xs of
| nil0() => stream_nil()
| cons0(x, xs) =>
  !(
    stream_append((sublist_sum(xs, m-x)).map(TYPE{intlist(m)})(lam ys => intlist_cons(x, ys)), sublist_sum(xs, m))
  ) (* end of [cons0] *)
)
//
) (* end of [sublist_sum] *)

(* ****** ****** *)
//
// HX: a bit of testing code
//
staload
UN = "prelude/SATS/unsafe.sats"
//
implement
main0() =
{
//
val xs =
g0ofg1($list{Int}(1,1,2,3,4))
//
val sols = sublist_sum(xs, 4)
val ((*void*)) = sols.foreach()(lam(xs) => println!($UN.cast{list0(int)}(xs)))
//
} (* end of [main0] *)
//
(* ****** ****** *)

(* end of [Q5_sol.dats] *)
