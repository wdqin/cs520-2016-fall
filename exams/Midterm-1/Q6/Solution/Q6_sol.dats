(* ****** ****** *)
//
#include "./../Q6.dats"
//
(* ****** ****** *)
//
stadef
AB_on(x:real, y:real): bool =
  (x1-x)*(y-y2) == (x-x2)*(y1-y)
//
stadef
AC_on(x:real, y:real): bool =
  (x1-x)*(y-y3) == (x-x3)*(y1-y)
//
stadef
BD_on(x:real, y:real): bool =
  (x2-x)*(y-y4) == (x-x4)*(y2-y)
//
stadef
CD_on(x:real, y:real): bool =
  (x3-x)*(y-y4) == (x-x4)*(y3-y)
//
(* ****** ****** *)

stacst xm: real
stacst ym: real // M: (xm, ym)
stacst xn: real
stacst yn: real // N: (xn, yn)

(* ****** ****** *)
//
extern
praxi AB_on_M(): [AB_on(xm, ym)] void
extern
praxi CD_on_N(): [CD_on(xn, yn)] void
//
(* ****** ****** *)
//
stadef
AN_on(x:real, y:real): bool =
  (x1-x)*(y-yn) == (x-xn)*(y1-y)
stadef
BN_on(x:real, y:real): bool =
  (x2-x)*(y-yn) == (x-xn)*(y2-y)
//
stadef
CM_on(x:real, y:real): bool =
  (x3-x)*(y-ym) == (x-xm)*(y3-y)
stadef
DM_on(x:real, y:real): bool =
  (x4-x)*(y-ym) == (x-xm)*(y4-y)
//
(* ****** ****** *)
//
stacst xp: real
stacst yp: real // P: (xp, yp)
stacst xq: real
stacst yq: real // Q: (xq, yq)
stacst xr: real
stacst yr: real // R: (xr, yr)
//
(* ****** ****** *)
//
extern
praxi AN_on_P(): [AN_on(xp, yp)] void
extern
praxi DM_on_P(): [DM_on(xp, yp)] void
//
(* ****** ****** *)
//
extern
praxi AC_on_Q(): [AC_on(xq, yq)] void
extern
praxi BD_on_Q(): [BD_on(xq, yq)] void
//
(* ****** ****** *)
//
extern
praxi BN_on_R(): [BN_on(xr, yr)] void
extern
praxi CM_on_R(): [CM_on(xr, yr)] void
//
(* ****** ****** *)
//
extern
prfun
lemma_quadrilateral
{
x1 < xm; xm < x2;
x4 < xn; xn < x3;
y4 < y1; y3 < y2;
y3 == 0; y4 == 0; true
} (): [colinear(xp, yp, xq, yq, xr, yr)] void
//
primplement
lemma_quadrilateral
  ((*void*)) = let
//
prval () = AB_on_M()
prval () = CD_on_N()
//
prval () = AC_on_Q()
prval () = BD_on_Q()
//
prval () = AN_on_P()
prval () = DM_on_P()
//
prval () = BN_on_R()
prval () = CM_on_R()
//
in
  // nothing
end // end of [lemma_quadrilateral]
//
(* ****** ****** *)

(* end of [Q6_sol.dats] *)
