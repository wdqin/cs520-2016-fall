(*
theCounter3_baconjs
*)

(* ****** ****** *)
//
#define
ATS_MAINATSFLAG 1
#define
ATS_DYNLOADNAME
"theCounter3_baconjs_start"
//
#define
ATS_STATIC_PREFIX "theCounter3_baconjs_"
//
(* ****** ****** *)
//
#include
"share/atspre_define.hats"
//
(* ****** ****** *)
//
#include
"{$LIBATSCC2JS}/staloadall.hats"
//
staload
"{$LIBATSCC2JS}/SATS/print.sats"
staload _(*anon*) =
"{$LIBATSCC2JS}/DATS/print.dats"
//
(* ****** ****** *)
//
staload
"{$LIBATSCC2JS}/SATS/Bacon.js/baconjs.sats"
//
(* ****** ****** *)
//
extern
fun
theShow_img_update(count: int): void = "mac#"
//
(* ****** ****** *)
//
#define N 12
//
implement
theShow_img_update
  (count) = let
//
fun
index2name
(
  index: int
) : string =
(
case+ index of
| 0 => "forest"
| 1 => "golden"
| 2 => "covered"
| 3 => "japanese"
| 4 => "pond-1"
| 5 => "trunks"
| 6 => "bridge"
| 7 => "river-1"
| 8 => "creek-2"
| 9 => "reflect-1"
| 10 => "reflect-2"
| _(*11*) => "Fall-Bike-Rides"
)
//
fun
img_update
(
  name: string
) : void =
{
//
val
theShow_img =
$extfcall(ptr, "jQuery", "#theShow_img")
//
val name_jpg = name + ".jpg"
//
val ((*void*)) =
$extmcall(void, theShow_img, "attr", "alt", name_jpg)
val ((*void*)) =
$extmcall(void, theShow_img, "attr", "src", "IMAGE/"+name_jpg)
//
} (* end of [img_update] *)
//
in
  img_update(index2name(count % N))
end // end of [theShow_img_update]

(* ****** ****** *)

local
//
datatype act = Next | Prev | Reset | Skip
//
val theNext_btn = $extval(ptr, "$(\"#theNext_btn\")")
val thePrev_btn = $extval(ptr, "$(\"#thePrev_btn\")")
val theReset_btn = $extval(ptr, "$(\"#theReset_btn\")")
//
val theNext_clicks = $extmcall(EStream(ptr), theNext_btn, "asEventStream", "click")
val thePrev_clicks = $extmcall(EStream(ptr), thePrev_btn, "asEventStream", "click")
val theReset_clicks = $extmcall(EStream(ptr), theReset_btn, "asEventStream", "click")
//
val theNext_clicks = theNext_clicks.map(TYPE{act})(lam _ => Next())
val thePrev_clicks = thePrev_clicks.map(TYPE{act})(lam _ => Prev())
val theReset_clicks = theReset_clicks.map(TYPE{act})(lam _ => Reset())
//
val theComb_clicks = merge(theNext_clicks, thePrev_clicks, theReset_clicks)
//
val theAuto_btn = $extval(ptr, "$(\"#theAuto_btn\")")
val theAuto_clicks = $extmcall(EStream(ptr), theAuto_btn, "asEventStream", "click")
val theAuto_clicks = theAuto_clicks.map(TYPE{act})(lam _ => Skip())
val theAuto_toggles = scan{bool}{act}(theAuto_clicks, false, lam(res, _) => ~res)
//
val () =
theAuto_toggles.onValue()
(
lam(tf) =>
if tf
then $extmcall(void, theAuto_btn, "addClass", "btn-primary")
else $extmcall(void, theAuto_btn, "removeClass", "btn-primary")
)
//
val theAutoComb_stream =
  Property_sampledBy_estream_cfun
    (theAuto_toggles, theComb_clicks, lam(x, y) => if x then Skip else y)
//
val theTick_stream =
  Property_sampledBy_estream
    (theAuto_toggles, Bacon_interval{int}(1500(*ms*), 0))
//
val theComb2_clicks = merge(theComb_clicks, theAuto_clicks)
val theComb2_property = EStream_toProperty_init(theComb2_clicks, Skip)
//
val theComb2Tick_stream =
  Property_sampledBy_estream_cfun
    (theComb2_property, theTick_stream, lam(x, y) => if y then x else Skip)
//
val
theCounts =
scan{int}{act}
(
  merge
  (
    theAutoComb_stream
  , theComb2Tick_stream
  )
, 0 (*initial*)
, lam(res, act) =>
  (
    case+ act of
    | Next() =>
      if res < 99 then res+1 else 0
    | Prev() =>
      if res > 1 then res-1 else 99
    | Reset() => (0) | Skip() => res
  )
) (* end of [theCounts] *)
//
in (* in-of-local *)
//
val () =
theCounts.onValue()
(
lam(count) => theShow_img_update(count)
) (* end of [val] *)
//
end // end of [local]

(* ****** ****** *)

%{^
//
var the_atsptr_null = 0;
//
%} // end of [%{$]
%{$
//
theCounter3_baconjs_start();
//
%} // end of [%{$]

(* ****** ****** *)

(* end of [theCounter3_baconjs.dats] *)
