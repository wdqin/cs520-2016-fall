(*
** Assignment 5
** It is due Wednesday, the 12th of October, 2016
*)

(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
#include
"share/HATS/atslib_staload_libats_libc.hats"
//
(* ****** ****** *)

staload $DIRENT

(* ****** ****** *)
//
extern
fun{}
myfind(path: string): void
extern
fun{}
myfind_recur(path: string): void
//
extern
fun{}
myfind_isdir(path: string): void
extern
fun{}
myfind_isreg(path: string): void
//
(* ****** ****** *)

implement
{}(*tmp*)
myfind(path) = let
//
val () =
println!
  ("myfind: path = ", path)
//
in
//
ifcase
  | test_file_isdir(path) > 0 => myfind_isdir(path)
  | test_file_isreg(path) > 0 => myfind_isreg(path)
  | _(*else*) => ()
//
end // end of [myfind]

(* ****** ****** *)
//
implement
{}(*tmp*)
myfind_recur = myfind<>
//
(* ****** ****** *)

staload UN = $UNSAFE

(* ****** ****** *)

implement
{}(*tmp*)
myfind_isdir
  (path) = let
//
val dirp = opendir_exn(path)
val ents = streamize_DIRptr_dirent(dirp)
//
fun
do_name
(
  name: string
) : void =
(
//
ifcase
  | name = "." => ()
  | name = ".." => ()
  | _(*else*) =>
    {
      val path2 =
        string0_append3(path, "/", name)
      // end of [val]
      val ((*void*)) =
        myfind_recur($UN.strptr2string(path2))
      // end of [val]
      val ((*freed*)) = strptr_free(path2)
    } (* end of [else] *)
//
) (* end of [do_name] *)
//
fun
fwork
(
  ent: &dirent
) : void = () where
{
  val (fpf|name) =
    dirent_get_d_name(ent)
  // end of [val]
  val ((*void*)) =
    do_name($UN.strptr2string(name))
  // end of [val]
  prval ((*returned*)) = fpf(name)
}
//
in
  stream_vt_foreach_cloptr(ents, lam(ent) => fwork(ent))
end // end of [myfind_isdir]

(* ****** ****** *)
//
implement
{}(*tmp*)
myfind_isreg(path) =
  println! ("myfind_isreg: path = ", path)
//
(* ****** ****** *)

implement
main0(argc, argv) =
{
//
val path =
(
  if argc >= 2 then argv[1] else "."
) : string
//
val () = myfind(path)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [myfind.dats] *)
